<?php

namespace Drupal\facets_taxonomy_multilevel\Plugin\facets\processor;

use Drupal\Core\Cache\UnchangingCacheableDependencyTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\taxonomy\TermStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a processor that provides dependent results on dependee facets.
 *
 * @FacetsProcessor(
 *   id = "term_dependent",
 *   label = @Translation("Show terms on Dependee Facet"),
 *   description = @Translation("Provides the results on term dependent facet. It is used with 'Show terms of defined depth'. Make sure depths are properly configured. It will work accurately if this processor is placed after the 'Show terms of defined depth'"),
 *   stages = {
 *     "build" = 41
 *   }
 * )
 */
class TermDependent extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  use UnchangingCacheableDependencyTrait;

  /**
   * The language manager.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager
   */
  protected $facetsManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $facetStorage;

  /**
   * The term storage service.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\facets\FacetManager\DefaultFacetManager $facets_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   The term storage service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DefaultFacetManager $facets_manager, EntityTypeManagerInterface $entity_type_manager, TermStorageInterface $term_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->facetsManager = $facets_manager;
    $this->facetStorage = $entity_type_manager->getStorage('facets_facet');
    $this->termStorage = $term_storage;
    ;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('facets.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $current_facet) {
    $build = [];

    $config = $this->getConfiguration();

    // Loop over all defined blocks and filter them by provider, this builds an
    // array of blocks that are provided by the facets module.
    /** @var \Drupal\facets\Entity\Facet[] $facets */
    $facets = $this->facetStorage->loadMultiple();
    foreach ($facets as $facet) {
      if ($facet->id() === $current_facet->id()) {
        continue;
      }

      $build[$facet->id()]['label'] = [
        '#title' => $facet->getName() . ' (' . $facet->getFacetSourceId() . ')',
        '#type' => 'label',
      ];

      $build[$facet->id()]['dependee'] = [
        '#title' => $this->t('Dependee'),
        '#type' => 'checkbox',
        '#default_value' => !empty($config[$facet->id()]['dependee']),
      ];
    }

    return parent::buildConfigurationForm($form, $form_state, $current_facet) + $build;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $conditions = $this->getConfiguration();
    $enabled_dependees = [];

    foreach ($conditions as $facet_id => $condition) {
      if (empty($condition['dependee'])) {
        continue;
      }
      $enabled_dependees[$facet_id] = $condition;
    }

    // Return as early as possible when there are no settings for allowed
    // facets.
    if (empty($enabled_dependees)) {
      return $results;
    }

    foreach ($enabled_dependees as $facet_id => $settings) {
      /** @var \Drupal\facets\Entity\Facet $current_facet */
      $current_facet = $this->facetStorage->load($facet_id);
      $current_facet = $this->facetsManager->returnBuiltFacet($current_facet);

      // Get the depth configuration from dependee facet.
      $current_facet_depth_level = $current_facet->getProcessorConfigs()['term_depth']['settings']['level'];
      // Get the bundle configuration from dependee facet.
      $current_facet_bundle = $current_facet->getProcessorConfigs()['term_depth']['settings']['bundle'];

      // Get active items.
      $active_items = $current_facet->getActiveItems();
      // Check whether the any active items present or not.
      if (count($active_items) > 0) {
        $valid_terms = [];
        foreach ($active_items as $tid) {
          $terms = $this->termStorage
            ->loadTree($current_facet_bundle, $tid, $current_facet_depth_level, TRUE);
          // Refine the results.
          foreach ($terms as $child_term) {
            $valid_terms[] = $child_term->id();
          }
        }
        /** @var \Drupal\facets\Result\ResultInterface $result */
        $filtered_results = array_filter($results, function ($result) use ($valid_terms) {
          return in_array($result->getRawValue(), $valid_terms);
        });

        $results = $filtered_results;
      }

      // Pass build processor information into current facet.
      $facet->addCacheableDependency($current_facet);
    }

    return $results;
  }

}
