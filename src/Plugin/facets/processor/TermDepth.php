<?php

namespace Drupal\facets_taxonomy_multilevel\Plugin\facets\processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\taxonomy\TermStorageInterface;
use Drupal\taxonomy\VocabularyStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processor that filters out results that doesn't belong to a defined depth.
 *
 * @FacetsProcessor(
 *   id = "term_depth",
 *   label = @Translation("Show terms of defined depth"),
 *   description = @Translation("Show only terms for a given level of depth"),
 *   stages = {
 *     "build" = 40
 *   }
 * )
 */
class TermDepth extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityTypeManager;

  /**
   * The term storage service.
   *
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $termStorage;

  /**
   * The vocabulary storage service.
   *
   * @var \Drupal\taxonomy\VocabularyStorageInterface
   */
  protected $vocabularyStorage;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\taxonomy\TermStorageInterface $term_storage
   *   The term storage service.
   * @param \Drupal\taxonomy\VocabularyStorageInterface $vocabulary_storage
   *   The vocabulary storage service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, TermStorageInterface $term_storage, VocabularyStorageInterface $vocabulary_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->termStorage = $term_storage;
    $this->vocabularyStorage = $vocabulary_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('entity_type.manager')->getStorage('taxonomy_vocabulary')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $processors = $facet->getProcessors();
    $config = $processors[$this->getPluginId()] ?? NULL;

    $min_level = 1;
    $build['level'] = [
      '#title' => $this->t('Level'),
      '#type' => 'number',
      '#min' => $min_level,
      '#default_value' => !is_null($config) ? $config->getConfiguration()['level'] : $min_level,
      '#description' => $this->t("Only show results for this level being 1 the root level."),
    ];

    $options = [];
    $vocabularies = $this->vocabularyStorage->loadMultiple();

    foreach ($vocabularies as $vid => $vocab) {
      $options[$vid] = $vocab->label();
    }
    $build['bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#default_value' => !is_null($config) ? $config->getConfiguration()['bundle'] : NULL,
      '#description' => $this->t("Choose vocabulary."),
      '#options' => $options,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results) {
    $processors = $facet->getProcessors();
    $config = $processors[$this->getPluginId()];
    $level = $config->getConfiguration()['level'];
    $depth = $level - 1;
    $vocabulary = $config->getConfiguration()['bundle'];

    try {
      $terms = $this->termStorage
        ->loadTree($vocabulary, 0, $level, FALSE);

      $valid_terms = [];
      foreach ($terms as $term) {
        if ($term->depth === $depth) {
          $valid_terms[] = $term->tid;
        }
      }

      /** @var \Drupal\facets\Result\ResultInterface $result */
      $filtered_results = array_filter($results, function ($result) use ($valid_terms) {
        return in_array($result->getRawValue(), $valid_terms);
      });

      $results = $filtered_results;
    }
    catch (\Exception $e) {
      $results = [];
    }

    return $results;
  }

}
