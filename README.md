# Facets taxonomy multilevel module

This module adds a processor to manage Taxonomy Depth for result display.
It also enables dependent facets to show children based on the selected parent
in the dependee facet.

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers

## Requirements

Adding new processors Term Depth and Term Dependent
for multilevel taxonomy facet.

## Recommended modules

This module used facets(https://www.drupal.org/project/facets) as a dependency.

## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules]
(https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Post Installation the facets will have additional processors configuration :
- Show terms of defined depth
- Show terms based on Dependee Facet

#### Note
- Processors exclusively apply to Taxonomy Fields.
- 'Show terms based on Dependee Facet' depends on 'Show terms of defined depth.'

## Maintainers

- Shubham Saurav Prasad (shubhamsprasad) - https://www.drupal.org/u/shubhamsprasad
- Sudipta Kumar Pal (skpal) - https://www.drupal.org/u/skpal
